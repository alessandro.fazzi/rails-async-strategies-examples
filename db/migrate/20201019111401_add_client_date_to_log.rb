class AddClientDateToLog < ActiveRecord::Migration[6.0]
def up
    add_column :logs, :client_date, :datetime
  end

  def down
    remove_column :logs, :client_date
  end
end

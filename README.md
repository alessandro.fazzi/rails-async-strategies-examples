The below described experiment is isolated into the commit https://gitlab.com/alessandro.fazzi/rails-async-strategies-examples/-/commit/63c9745745b7b722ee6b767671fb20608383dab7

# Which strategies?

* **ActionCable** is used to fire actions from the client to the server and vice versa
* database writes are delegated to an async queue using **ActiveJob**
* in the front-end **RxJS** is used to execute logic based upon events w/o juggling with timing issues.
  thanks to the reactive pattern we're coordinating front-end user-generated events with backend
  operations in a straightforward and readable way.

# Flow notes

1. we arrive on the home page
1. two textareas are rendered on the left and on the right. The one on the right is readonly.
   we can write into the one on the left
1. when the page is loaded `app/javascript/channels/writing_channel.js` creates a gloabl `Channel`
   object. It is a consumer subscribed to `WritingChannel` publisher
1. `app/javascript/channels/writing_channel.js`'s `initialized` callback is immediately fired
  here we only manage the button to enable browser notifications. Native browser notification
  are here just to experiment with the API
1. in `app/javascript/packs/application.js` we have the whole small RxJS experiment. Using Rx we
  _observe_ the textarea on the left. Everytime we press `Enter` our _observer_ will send the text
  from the previous line (wrote just before the "enter") and will call `.server_log` on the global
  `Channel` object
1. `app/channels/writing_channel.rb`'s `#server_log` is called passing parameters (`{body: some_string}`)
1. `PersistLogJob` is called asyncronously using an ActiveJob queue. The goal is to have non-blocking
  database interactions and go back to the client as fast as possible
1. `WritingChannel.broadcast_to(params[:user], data)` il called. `params` are not passed as arguments:
  they are already inside the `Channel` consumer. `data` is just getting proxied from arguments
1. when a `broadcast_to` is performed, the `received` callback is fired on the `Channel` consumer
1. `.received` will send a notification to the browser
1. `.received` will receive the text through arguments and will write it in the textarea on the right

class PersistLogJob < ApplicationJob
  class TheMinuteIsOddError < StandardError; end

  queue_as :database
  retry_on TheMinuteIsOddError, wait: 15.seconds, attempts: 5

  def perform(text:, client_date:)
    if Time.now.min.odd?
      MessageNotPersistedChannel.broadcast_to('user1', client_date)
      raise TheMinuteIsOddError
    else
      MessagePersistedChannel.broadcast_to('user1', client_date)
      Log.new(text: text, client_date: client_date).save
    end
  end
end

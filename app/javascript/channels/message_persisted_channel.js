import consumer from "./consumer"

consumer.subscriptions.create(
  { channel: "MessagePersistedChannel", user: 'user1' },
  {
    connected() {
      console.log('persisted in ascolto')
    },

    disconnected() {
      // Called when the subscription has been terminated by the server
    },

    received(message_id) {
      window.persistedList.next(message_id)
    }
  }
);

import consumer from "./consumer"

consumer.subscriptions.create(
  { channel: "MessageNotPersistedChannel", user: 'user1' },
  {
    connected() {
      console.log('pending in ascolto')
    },

    disconnected() {
      // Called when the subscription has been terminated by the server
    },

    received(message_id) {
      window.pendingList.next(message_id)
    }
  }
);

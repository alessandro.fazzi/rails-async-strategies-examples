import consumer from "./consumer"
import Message from "../packs/message"

// Make this Channel object available globally. Rude, but we don't mind to fine tune
// this implementation's aspect
window.Channel = consumer.subscriptions.create(
  // +user+ key will be a +param+ into WritingChannel ruby class
  { channel: "WritingChannel", user: 'user1' },
  {
    // called when something is broadcasted from the server
    received(data) {
      console.log(data);
      if(Notification.permission !== 'granted') {
        return false;
      }

      // browser native notification
      new Notification('Log ricevuto');
      this.log(data.date, data.body).then((el) => {
        // focusing on second textarea to follow scroll down
        el.focus()
        // focusing back so the user can continue writing
        document.querySelector('#user1').focus()
      })
    },
    initialized() {
      console.log('Initialized');
      document.addEventListener('DOMContentLoaded', () => {
        // Button to enable browser notifications
        document.querySelector('#accept_notifications').addEventListener('click', (ev) => {
          Notification.requestPermission()
            .then((permission) => console.log(`L'utente ha settato il permesso per le notifiche: ${permission}`))
        })
      })
    },
    log(date, text) {
      return new Promise((resolve, reject) => {
        let message = new Message(date, text)
        message.append()
        resolve(message.element)
      })
    },
    server_log(data) {
      // `this.perform` will call the required method as defined into the ActionCable class
      this.perform('server_log', data)
    }
  }
)

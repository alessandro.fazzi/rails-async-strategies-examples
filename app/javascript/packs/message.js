class LogElement {
  constructor(date, text) {
    date = document.createTextNode(date);
    text = document.createTextNode(text);
    let dateEl = document.createElement('p');
    dateEl.appendChild(date);
    let textEl = document.createElement('p');
    textEl.appendChild(text);
    let li = document.createElement('li');
    li.setAttribute('data-date', date.textContent);
    li.appendChild(dateEl);
    li.appendChild(textEl);

    this.element = li
  }

  append() {
    document.querySelector('#user2').appendChild(this.element)
  }
}

export default LogElement

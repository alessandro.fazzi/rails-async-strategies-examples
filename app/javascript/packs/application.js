// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")


// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)

import { fromEvent, from, Subject } from 'rxjs';
import { buffer, filter, map } from 'rxjs/operators';

document.addEventListener("DOMContentLoaded", () => {
  document.querySelectorAll('#user1, #user2').forEach(el => el.value = null)
});
const dictionary = [
  'suca',
  'vaffanculo',
  'perfetto'
]

const keyboardInputs = fromEvent(document, 'keyup').pipe(map(ev => ev.key))
const userText = keyboardInputs.pipe(filter(key => /^[\w -.,:;]$/.test(key)))
const enter = keyboardInputs.pipe(filter(key => key == 'Enter'))
const bufferedText = userText.pipe(buffer(enter))
const politeText = bufferedText.pipe(map(string => {
  const badWordsPresent = dictionary.reduce((accumulator, badWord) => accumulator == string.join('').includes(badWord), true)
  if (true == badWordsPresent) {
    return '[REDACTED]';
  } else {
    return string.join('');
  }
}))
politeText.subscribe(x => {
  // Keep in mind: we're working w/o timezone right now
  window.Channel.server_log({ body: x, date: new Date().toISOString() })
})

window.pendingList = new Subject();
window.persistedList = new Subject();

pendingList.subscribe({
  next: (v) => {
    let message = document.querySelector('[data-date="' + v + '"]')
    message.style.backgroundColor = 'orange'
  }
});

persistedList.subscribe({
  next: (v) => {
    let message = document.querySelector('[data-date="' + v + '"]')
    message.style.backgroundColor = 'green'
  }
});

class HomeController < ApplicationController
  def new
    logs = Log.order(:client_date).last(10)
    render 'new', locals: { logs: logs }
  end
end

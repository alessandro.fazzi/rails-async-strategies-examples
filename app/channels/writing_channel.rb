class WritingChannel < ApplicationCable::Channel
  def subscribed
    stream_from "writing:#{params[:user]}"
    Rails.logger.info 'Subscription arrived; params: ' + params.to_s
  end

  def receive(data)
    # this method will be automatically invoked when `Channel.send` method is used on
    # the front-end. For experimental pourpose I prefere to use custom named methods
    # over implicit callbacks. I also found that implicit callbacks are not enough
    # well documented in Rails Guides
  end

  def server_log(data)
    Rails.logger.info 'Data received; params: ' + params.to_s + ' data: ' + data.to_s
    # Async job to save on DB
    PersistLogJob.perform_later(text: data['body'], client_date: data['date'])
    # rebroadcast to the same Channel. In this implementation we have
    # just one channel; when we receive data on the backend, we re-broadcast them
    # to front-end
    self.class.broadcast_to(params[:user], data)
  end
end

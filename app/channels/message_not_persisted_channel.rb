class MessageNotPersistedChannel < ApplicationCable::Channel
  def subscribed
    stream_from "message_not_persisted:#{params[:user]}"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def receive(data)
    Rails.logger.info data
  end
end

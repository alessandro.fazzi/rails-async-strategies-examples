class MessagePersistedChannel < ApplicationCable::Channel
  def subscribed
    stream_from "message_persisted:#{params[:user]}"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end

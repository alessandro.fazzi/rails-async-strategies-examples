module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user # This name could be anything we want

    def connect
      self.current_user = current_role
    end

    def disconnect
      # Any cleanup work needed when the cable connection is cut.
    end

    private

    # We have not authentication in this experiment, so we're recognizing
    # any anonymous user as `writer`
    def current_role
      'writer'
    end
  end
end
